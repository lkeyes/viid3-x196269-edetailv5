// --- local.js --- //

com.gsk.mt.onInit = function() {
	$(".isi-right #collapse-btn").trigger("click");
    $(".isi-right #expand-btn").show();
    $(".isi-right #collapse-btn").hide();

    com.gsk.mt.customSwipe({
        "rightSlide": "DLLEDTL200002_DOVATO_2020_US5.0_002",
        "leftSlide": "DLLEDTL200002_DOVATO_2020_US5.0_004"
	});
};

$(document).ready(function(){

    $("#tab_1_button").on("click", function() {
        const chartAnimProps = {transformOrigin: 'center bottom', scaleY: 0, autoAlpha: 0, y: 0, height: 0};
        const gradsAnimProps = {transformOrigin: 'center bottom', scaleY: 0, autoAlpha: 0, y: '-65', height: 0};
        const shiftedBarsProps = {transformOrigin: 'center bottom', scaleY: 0, autoAlpha: 0, y:'15', height: 0};
        const shiftedBarsProps2 = {transformOrigin: 'center bottom', scaleY: 0, autoAlpha: 0, y:'30', height: 0};

        const allBars = [   
                            '#pink-bar-91', 
                            '#gray-bar-93',
                            '#pink-bar-91--shadow', 
                            '#gray-bar-93--shadow', 
                            '#pink-bar-86', 
                            '#gray-bar-89-5',
                            '#pink-bar-86--shadow', 
                            '#gray-bar-89-5--shadow', 
                            '#pink-bar-3--shadow', 
                            '#gray-bar-2--shadow',
                            '#pink-bar-3-1--shadow',
                            '#gray-bar-2-0--shadow',
                            '#pink-bar-6--shadow',
                            '#gray-bar-5--shadow',
                            '#pink-bar-10-9--shadow', 
                            '#gray-bar-8-5--shadow'
                        ];

        const shiftedBars = [
                            '#pink-bar-3', 
                            '#gray-bar-2',
                            '#pink-bar-3-1',
                            '#gray-bar-2-0',
                            '#pink-bar-6',
                            '#gray-bar-5'
                        ];      
        
        const shiftedBars2 = [
                            '#pink-bar-10-9',
                            '#gray-bar-8-5'
                        ];  

        const barGradients = [
                            '#pink-bar-91--grad', 
                            '#gray-bar-93--grad', 
                            '#pink-bar-3--grad',
                            '#gray-bar-2--grad', 
                            '#pink-bar-6--grad',
                            '#gray-bar-5--grad', 
                        ];
        const tweenDuration = 1;

        var chartsAnim = new TimelineMax();
        var gradsAnim = new TimelineMax();
        var shiftedBarAnim = new TimelineMax();
        var shiftedBarAnim2 = new TimelineMax();

        chartsAnim.from(allBars, tweenDuration, chartAnimProps);
        gradsAnim.from(barGradients, tweenDuration, gradsAnimProps);
        shiftedBarAnim.from(shiftedBars, tweenDuration, shiftedBarsProps);
        shiftedBarAnim2.from(shiftedBars2, tweenDuration, shiftedBarsProps2);
    });
   

    $("#tab_3_button").on("click", function() {
        var point_1 = new TimelineMax();
        var point_2 = new TimelineMax();
        var point_3 = new TimelineMax();
        var point_4 = new TimelineMax();
        var point_5 = new TimelineMax();
        var point_6 = new TimelineMax();
        var point_7 = new TimelineMax();

        var grey_circle = new TimelineMax();
        var pink_circle = new TimelineMax();

        var scaleIncrease = 3;
        var inOutDuration = .15;
        var centerOrig = "center center";
        
        // Pink line
       TweenMax.fromTo("#svg_pink_line", 3, {drawSVG:"100% 100%"}, {drawSVG:"100% 0%"});
        point_1.fromTo("#svg_point_1", inOutDuration, {scale:1, transformOrigin:centerOrig, opacity:0}, {delay:.35, scale:scaleIncrease, transformOrigin:centerOrig, opacity:1})
        .to("#svg_point_1", inOutDuration, {scale:1, transformOrigin:centerOrig});
        point_2.fromTo("#svg_point_2", inOutDuration, {scale:1, transformOrigin:centerOrig, opacity:0}, {delay:.5, scale:scaleIncrease, transformOrigin:centerOrig, opacity:1})
        .to("#svg_point_2", inOutDuration, {scale:1, transformOrigin:centerOrig});
        point_3.fromTo("#svg_point_3", inOutDuration, {scale:1, transformOrigin:centerOrig, opacity:0}, {delay:.6, scale:scaleIncrease, transformOrigin:centerOrig, opacity:1})
        .to("#svg_point_3", inOutDuration, {scale:1, transformOrigin:centerOrig});
        point_4.fromTo("#svg_point_4", inOutDuration, {scale:1, transformOrigin:centerOrig, opacity:0}, {delay:.7, scale:scaleIncrease, transformOrigin:centerOrig, opacity:1})
        .to("#svg_point_4", inOutDuration, {scale:1, transformOrigin:centerOrig});
        point_5.fromTo("#svg_point_5", inOutDuration, {scale:1, transformOrigin:centerOrig, opacity:0}, {delay:1.05, scale:scaleIncrease, transformOrigin:centerOrig, opacity:1})
        .to("#svg_point_5", inOutDuration, {scale:1, transformOrigin:centerOrig});
        point_6.fromTo("#svg_point_6", inOutDuration, {scale:1, transformOrigin:centerOrig, opacity:0}, {delay:1.6, scale:scaleIncrease, transformOrigin:centerOrig, opacity:1})
        .to("#svg_point_6", inOutDuration, {scale:1, transformOrigin:centerOrig});
        point_7.fromTo("#svg_point_7", inOutDuration, {scale:1, transformOrigin:centerOrig, opacity:0}, {delay:2.9, scale:scaleIncrease, transformOrigin:centerOrig, opacity:1})
        .to("#svg_point_7", inOutDuration, {scale:1, transformOrigin:centerOrig});

        // Grey line
        TweenMax.fromTo("#svg_grey_line", 3, {drawSVG:"0%"}, {drawSVG:"100%"});
        grey_circle.fromTo("#grey_circle", inOutDuration, {scale:1, transformOrigin:centerOrig}, {delay:2.9, scale:1.4, transformOrigin:centerOrig})
        .to("#grey_circle", inOutDuration, {scale:1, transformOrigin:centerOrig});
        pink_circle.fromTo("#pink_circle", inOutDuration, {scale:1, transformOrigin:centerOrig}, {delay:2.9, scale:1.4, transformOrigin:centerOrig})
        .to("#pink_circle", inOutDuration, {scale:1, transformOrigin:centerOrig});

    }); 

    $("#tab_4_button").on("click", function() {

        const chartAnimPropsTabFour = {transformOrigin: 'center bottom', scaleY: 0, autoAlpha: 0, y: 0, height: 0};
        const gradsAnimPropsTabFour = {transformOrigin: 'center bottom', scaleY: 0, autoAlpha: 0, y: '-65', height: 0};

        const allBarsTabFour = [   
            '#pink-bar-91-t4', 
            '#pink-bar-91--shadow-t4', 
            '#gray-bar-94-t4',
            '#gray-bar-94--shadow-t4', 
            '#pink-bar-87-t4',
            '#pink-bar-87--shadow-t4',
            '#gray-bar-90-t4',
            '#gray-bar-90--shadow-t4',
            '#pink-bar-92-t4',
            '#pink-bar-92--shadow-t4',
            '#gray-bar-90-2-t4',
            '#gray-bar-90-2--shadow-t4',
            '#pink-bar-84-t4', 
            '#pink-bar-84--shadow-t4',
            '#gray-bar-86-t4',
            '#gray-bar-86--shadow-t4' 
        ];

        const barGradientsTabFour = [
            '#pink-bar-92--grad-t4', 
            '#gray-bar-90--grad-t4' 
        ];
    
        const tweenDurationTabFour = 1;

        var chartsAnimTabFour = new TimelineMax();
        var gradsAnimTabFour = new TimelineMax();

        chartsAnimTabFour.from(allBarsTabFour, tweenDurationTabFour, chartAnimPropsTabFour);
        gradsAnimTabFour.from(barGradientsTabFour, tweenDurationTabFour, gradsAnimPropsTabFour);

    });
 
    $("#tab_1_button").trigger("click");

    if (localStorage.getItem('tab') == 2) {
        $("#tab_2_button").trigger("click");
    }

    localStorage.clear();

		$('.side-menu__item').on("click", function(){
			if ( $('div.tab-2').hasClass('active') ) {
				$('#container').css('background-position', '0 -80px')
			} else if ( $('div.tab-1').hasClass('active') ) {
				$('#container').css('background-position', '0 0')
			}
		});
});


// Custome menu code
$(document).ready(function() { com.gsk.mt.initialise(); });
$('#customMenu .customMenu .gotoSlide').click (
     
     $e => {
         $e.preventDefault();
         let slide;
         const slideSection = $e.target.dataset.slideSection;
         
         if (!$e.target.dataset.slide) {
             const parentSlide = $($e.target).closest('.section')[0];
             slide = parentSlide.dataset.slide;
         } else {
             slide = $e.target.dataset.slide
         }
         
         if (slide) {
             com.gsk.mt.gotoSlide(slide);
         }
     }
 );
