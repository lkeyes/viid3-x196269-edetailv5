/**********************************************************/
/* GSK Veeva Master Template - Presentation Functionality */
/**********************************************************/
/* File version              1.6.6                        */
/* Last modified             08/06/2018                   */
/* Last modified by          Pulse                        */
/**********************************************************/

// --- CUSTOM TO THIS PRESENTATION --- //

var PRODUCTION = true; // UPDATE FOR PRODUCTION BUILDS

const menuItems = ["#home", "#references", "#Isi-nav", "#video"];
const disabledItems = ["noHome", "noReferences" ];
let disabledMenuItems = [];

//Toggle to window.innerWidth in dev reposnive mode
const scrW = screen.width; //window.innerWidth;

if ( scrW < 1366) {
    var scale = scrW / 1024;
    var translateY = '-.75%';
} else {
    var scale = 1024 / scrW;
    var translateY = '-.3%';
}

var scaleString ='scale(' + scale + ') translate(-50%, -50%)';
var scale ='scale(' + scale + ')';
var translate =`translate(-50%,${translateY})`;
var translateDialog ='translate(-170px, 0)';

var openDialog = false;

$("body").css({'transform': scale, '-webkit-transform': scale});
$("#container").css({'transform': translate, '-webkit-transform': translate});

if (!PRODUCTION) {
    $("body").css({'transform': 'none', '-webkit-transform': 'none', 'top': 'initial', 'left': 'initial'});
    $("#container").css({'transform': 'none', '-webkit-transform': 'none', 'top': 'initial', 'left': 'initial'});
    $(".ui-widget-overlay").css({'transform': 'none', '-webkit-transform': 'none', 'top': 'initial', 'left': 'initial'});
    $(".ui-dialog").css({'transform': 'none', '-webkit-transform': 'none', 'top': 'initial', 'left': 'initial'});
}

// On dialog open
com.gsk.mt.onDialogOpen = function() {
    openDialog = true;

    if ($("#menu").hasClass("activated")) {
        menuToggle();
    }

    if (PRODUCTION) {
        $(".ui-dialog").css({'transform': translateDialog, '-webkit-transform': translateDialog});
        $(".ui-widget-overlay").css({'transform': translateDialog, '-webkit-transform': translateDialog});
    }
}

// On dialog close
com.gsk.mt.onDialogClose = function() {
    openDialog = false;

    for (var i = 0; i < menuItems.length; i++) {
        deactivate($(menuItems[i]));
    }

    if (com.gsk.mtconfig.enableQuickLinksOnDialog) {
        $(".navBottom").appendTo(com.gsk.mt.dom.container);
    }

    disable();

    if (PRODUCTION) {
        $(".ui-widget-overlay").remove();
    }
}


function activate(args) {
    openDialog = true;
    $(args).removeClass("deactivated");
    $(args).addClass("activated");
}

function deactivate(args) {
    openDialog = false;
    $(args).removeClass("activated");
    $(args).addClass("deactivated");
    $(args).removeClass("activeNav");
}

function close() {
    for (var i = 0; i < menuItems.length; i++) {
        deactivate($(menuItems[i]));
    }

    $(".ui-dialog-titlebar-close").each(function() {
        $(this).trigger("click");
    });
}

function toggle(args) {
    if (disabledMenuItems.includes(args)) {
       disable();
    } else {
        // If deactivated
        if ($(args).hasClass("deactivated")) {
            activate(args);
        } else {
            close();
        }
    }

    disable();
}

function hasDisabledMenuItems() {
    for (var i = 0; i < menuItems.length; i++) {
        for (var j = 0; j < disabledItems.length; j++) {
            if ($(menuItems[j]).hasClass(disabledItems[j])) {
                if (disabledMenuItems.includes(menuItems[j])) {} else {
                    disabledMenuItems.push(menuItems[j]);
                }
            }
        }
        console.log(disabledMenuItems);
        return true;
    }
    return false;
}

function disable() {
    for (var i = 0; i < disabledMenuItems.length; i++) {
        $(disabledMenuItems[i]).removeClass("openDialog");
        $(disabledMenuItems[i]).removeClass("gotoFlow");
        $(disabledMenuItems[i]).addClass("activeNav");
    }
    // console.log(disabledMenuItems);
}

function menuToggle() {
    // Set activated & deactivated
    if ($("#menu").hasClass("deactivated")) {
        $("#menu").removeClass("deactivated");
      //  $("#menu").addClass("activated");
    } else {
        $("#menu").removeClass("activated");
      //  $("#menu").addClass("deactivated");
    }

    // If deactivated
    if ($("#menu").hasClass("deactivated")) {
        $("#menu").removeClass("activeNav");
        $("#customMenuWrapper").hide();

        if (com.gsk.mtconfig.enableQuickLinksOnDialog) {
            $(".navBottom").appendTo(com.gsk.mt.dom.container);
        }
    }

    disable();
}


// Refactoring repetitive code
const tabClickFn = (prefix, n)=>{
    let el = `#${prefix}_${n}_button`;
    let h1 = `.h1-header--tab`;
    let elHeader = `${prefix}_${n}_header`;
    let sideMenu = `.side-menu__item`;
    let elTab = (prefix == 'tab')?`.tab`: `.profile_tab`;
    let elTabN = (prefix == 'tab')? `.tab-${n}`: `.profile_tab-${n}`;

    $(el).click(()=>{
        $(h1).removeClass("active");
        $(elHeader).addClass("active"); 
        $(sideMenu).removeClass("active");
        $(el).addClass("active");  
        $(elTab).removeClass("active");
        $(elTabN).addClass("active"); 

        //specific for profile tabs

        if(prefix == 'profile'){
            $(".top-menu__item").removeClass("active");
            $(".top-menu__item").removeClass("inactive");

            if(n==1){
                $("#profile_2_button").addClass("inactive");
                $("#profile_1_button").addClass("active"); 
            }

            if(n==2){
                $("#profile_1_button").addClass("inactive");
                $("#profile_2_button").addClass("active");  
            }
        }
    })
}





$(document).ready(function() {

    $("#container").css('opacity', '1');

     $("#pop-tabs_1_button").on("click", function() {
        $(".top-menu__item").removeClass("active");
        $(".top-menu__item").removeClass("inactive");
        $("#pop-tabs_2_button").addClass("inactive");
        $("#pop-tabs_1_button").addClass("active"); 
        $(".pop-tab").removeClass("active");
        $(".pop-tab-1").addClass("active"); 
    });
    $("#pop-tabs_2_button").on("click", function() {
        $(".top-menu__item").removeClass("active");
        $(".top-menu__item").removeClass("inactive");
        $("#pop-tabs_1_button").addClass("inactive");
        $("#pop-tabs_2_button").addClass("active");  
        $(".pop-tab").removeClass("active");
        $(".pop-tab-2").addClass("active"); 
    });


    $(".isi-right #expand-btn").on("click", function(){ 
        if($(".isi-footer").hasClass("tivicay")) {
            $(".isi-footer").css("height","84%");
            $("#tray_isi").addClass("paddingFull");
        } else {
            $(".isi-footer").css("height","32.5%");
        }
        $("#expand-btn").toggle();
        $("#collapse-btn").toggle();
    });
    $(".isi-right #collapse-btn").on("click", function(){
        $(".isi-footer").css("height","17%");
        $("#expand-btn").toggle();
        $("#collapse-btn").toggle();
        if($(".isi-footer").hasClass("tivicay")) {
            $("#tray_isi").removeClass("paddingFull");
        }
    });


    tabClickFn('tab', 1);
    tabClickFn('tab', 2);
    tabClickFn('tab', 3);
    tabClickFn('tab', 4);

    $("#pi").removeClass("openDialog");


    tabClickFn('profile', 1);
    tabClickFn('profile', 2);

    // On dialog close
    $("[aria-describedby*='dialog-patient-profiles'] .ui-dialog-titlebar-close").on("click", function(){ 
        $(".top-menu__item").removeClass("active");
        $(".top-menu__item").removeClass("inactive");
        $(".profile_tab").removeClass("active");
        $(".profile_tab").removeClass("inactive");
        $(".profile_tab-reset").addClass("active");
    });


    $("[aria-describedby*='dialog-t-cell-count'] .ui-dialog-titlebar-close").on("click", function(){ 
        $("#pop-tabs_1_button").removeClass("active");
        $("#pop-tabs_2_button").removeClass("active");
        $("#pop-tabs_1_button").removeClass("inactive");
        $("#pop-tabs_2_button").removeClass("inactive");
    });
    
    // Email logic 
    // setFragments();
    $('#email').click(launchEmail);

    

    // On references menu click
  if(!$("#references").hasClass("noReferences")) {
    com.gsk.mt.dom.body.on(com.gsk.mt.releaseEvent, "#references", function() {
        toggle("#references");
    });
    }

    //On video menu click
    com.gsk.mt.dom.body.on(com.gsk.mt.releaseEvent, "#video", function() {
        toggle("#video");
    });

    // On menu click
    com.gsk.mt.dom.body.on(com.gsk.mt.releaseEvent, "#menu", function() {
        menuToggle();
    });

    //  On menu close button click
    com.gsk.mt.dom.body.on(com.gsk.mt.releaseEvent, "#closeCustomMenu", function() {
        menuToggle();
    });
    
    // On Isi nav menu click
    com.gsk.mt.dom.body.on(com.gsk.mt.releaseEvent, "#Isi-nav", function() {
        toggle("#Isi-nav");
    });
    
    // On close button click
    com.gsk.mt.dom.body.on(com.gsk.mt.releaseEvent, ".ui-dialog-titlebar-close", function() { 
        close();
    });


    var videoD3 = document.getElementById("d3-video");
    videoD3.pause();
    videoD3.currentTime = 1.5;

	$('button.ui-dialog-titlebar-close').click(function(e) {
		videoD3.pause();
		videoD3.currentTime = 1.5;
	});

});



function setFragments() {

    var fragments = JSON.stringify(com.gsk.mtconfig.fragments.map(frag => {
        return frag.id;
    }));
    
    window.sessionStorage.setItem('mtgskEmailFragments', fragments);
}

function launchEmail () {
   //  console.log('in the launch function', com.gsk.mtconfig);
    let templateID = com.gsk.mtconfig.templateID;
    com.veeva.clm.launchApprovedEmail(templateID,"",function(r){
    //  console.log('launching email:', r);
    });
}


// Custom Menu Behavior

    $('.customMenu .baseline').click(() => {
        sessionStorage.setItem('slideSection', 'baseline-characteristics');
        setTimeout(() => {
            com.gsk.mt.gotoSlide('DLLEDTL200002_DOVATO_2020_US5.0_002');
        }, 100);
    });