// This implementation does not guarantee all content/dialogs have been injected before the mt parses and attaches functionality. Switching to promise to properly implement.
// $.holdReady( true );
// $(".dialogVideo").load("../shared/media/content/dialogVideo.html", function(){
//     $.holdReady( false );
// });

// let $dialogVideo = $('<div id="dialog-video" style="padding: 0; align-items: center; justify-content: center" class="dialog noTitlebar hidden videoDialog" data-description="Parent popup"></div>');
// $($dialogVideo).load('../shared/media/content/dialogs/dialogVideo.html', function(){
//     $('#container').append($dialogVideo);
//     $.holdReady( false );
// });

// let $dialogM184VVideo = $('<div id="dialogM184VVideo" style="padding: 0; align-items: center; justify-content: center" class="dialog noTitlebar hidden videoDialog" data-description="Parent popup"></div>');
// $($dialogM184VVideo).load('../shared/media/content/dialogs/dialogM184VVideo.html', function(){
//     $('#container').append($dialogM184VVideo);
//     $.holdReady( false );
// });


// let $dialogISI = $('<div id="safe-dialog" style="padding: 0; align-items: center; justify-content: center" class="dialog noTitlebar hidden scrollable" data-description="Parent popup"></div>');
// $($dialogISI).load('../shared/media/content/dialogs/dialogISI.html', function(){
//     $('#container').append($dialogISI);
//     $.holdReady( false );
// });

// let $dialogPatientProfiles = $('<div id="dialog-patient-profiles" class="dialog noTitlebar hidden" data-description="Parent popup"></div>');
// $($dialogPatientProfiles).load('../shared/media/content/dialogs/dialogPatientProfiles.html', function(){
//     $('#container').append($dialogPatientProfiles);
//     $.holdReady( false );
// });

// let $dialogTCellCount = $('<div id="dialog-t-cell-count" class="dialog noTitlebar hidden" data-description="Parent popup"></div>');
// $($dialogTCellCount).load('../shared/media/content/dialogs/dialogTCellCount.html', function(){
//     $('#container').append($dialogTCellCount);
//     $.holdReady( false );
// });




// Tell GSK MT to wait (via jQuery doc ready emitter)
$.holdReady(true);

let elementPromises = [];

// Put elements here
const globalElements = [
    // this appears to not even exist
    // {
    //     selector: '.dialogVideo',
    //     path: '../shared/media/content/dialogVideo.html'
    // }
];

const dialogs = [
    {
        selector: '<div id="dialog-video" style="padding: 0; align-items: center; justify-content: center" class="dialog noTitlebar hidden videoDialog" data-description="Parent popup"></div>',
        path: '../shared/media/content/dialogs/dialogVideo.html'
    },
    {
        selector: '<div id="dialogM184VVideo" style="padding: 0; align-items: center; justify-content: center" class="dialog noTitlebar hidden videoDialog" data-description="Parent popup"></div>',
        path: '../shared/media/content/dialogs/dialogM184VVideo.html'
    },
    {
        selector: '<div id="safe-dialog" style="padding: 0; align-items: center; justify-content: center" class="dialog noTitlebar hidden scrollable" data-description="Parent popup"></div>',
        path: '../shared/media/content/dialogs/dialogISI.html'
    },
    {
        selector: '<div id="dialog-patient-profiles" class="dialog noTitlebar hidden" data-description="Parent popup"></div>',
        path: '../shared/media/content/dialogs/dialogPatientProfiles.html'
    },
    {
        selector: '<div id="dialog-t-cell-count" class="dialog noTitlebar hidden" data-description="Parent popup"></div>',
        path: '../shared/media/content/dialogs/dialogTCellCount.html'
    },
];

// Iterate over elements and generate promises for each
globalElements.forEach(el => {
    let loaderPromise = new Promise((resolve, reject) => {
        $(el.selector).load(el.path, () => {
            resolve();
        });
    });
    elementPromises.push(loaderPromise);
});

dialogs.forEach(el => {
    let loaderPromise = new Promise((resolve, reject) => {
        let dialog = $(el.selector);
        $(dialog).load(el.path, () => {
            $('.mainContent').append(dialog);
            resolve();
        });
    });
    elementPromises.push(loaderPromise);
});



// Await resolution of all promises before allowing docready to fire
Promise.all(elementPromises).then(() => {
    // console.log('All elements loaded');
    $.holdReady(false);
});
