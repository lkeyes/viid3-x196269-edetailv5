$.holdReady( true );
$(".dialogVideo").load("../shared/media/content/dialogVideo.html", function(){
    $.holdReady( false );
});

let $dialogVideo = $('<div id="dialog-video" style="padding: 0; align-items: center; justify-content: center" class="dialog noTitlebar hidden" data-description="Parent popup"></div>');
$($dialogVideo).load('../shared/media/content/dialogs/dialogVideo.html', function(){
    $('#container').append($dialogVideo);
    $.holdReady( false );
});

let $dialogISITrivicay = $('<div id="safe-dialog-trivicay" style="padding: 0; align-items: center; justify-content: center" class="dialog noTitlebar hidden scrollable" data-description="Parent popup"></div>');
$($dialogISITrivicay).load('../shared/media/content/dialogs/dialogISI_trivicay.html', function(){
    $('#container').append($dialogISITrivicay);
    $.holdReady( false );
})


