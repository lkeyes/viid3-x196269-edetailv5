// --- local.js --- //

com.gsk.mt.onInit = function() {
    $(".isi-right #collapse-btn").trigger("click");
    $(".isi-right #expand-btn").show();
    $(".isi-right #collapse-btn").hide();

    com.gsk.mt.customSwipe({
        "rightSlide": "DLLEDTL200002_DOVATO_2020_US5.0_008",
        "leftSlide": "DLLEDTL200002_DOVATO_2020_US5.0_010"
	});
};

$(document).ready(function() {

    // $("#container").on("swipeleft",function(){
    //     $(this).addClass("noSwipe");
    // });

    $("#container").on("swiperight",function(){
        $(this).removeClass("noSwipe");
    });


    var wrapper = document.getElementById('wrapper');
    var myScroll = new IScroll(wrapper, { scrollY: true, scrollbars: 'custom' });
    myScroll.disable();

    $("#isi-footer .iScrollVerticalScrollbar.iScrollLoneScrollbar").addClass('collapsed');

    $(".isi-right #expand-btn").on("click", function() {
        myScroll.enable();
        $("#isi-footer .iScrollVerticalScrollbar.iScrollLoneScrollbar").removeClass('collapsed');
    });

    $(".isi-right #collapse-btn").on("click", function() {
        myScroll.scrollTo(0, 0);
        $("#isi-footer .iScrollVerticalScrollbar.iScrollLoneScrollbar").addClass('collapsed');
        myScroll.disable();
    });

});

// Custome menu code
$(document).ready(function() { com.gsk.mt.initialise(); });
$('#customMenu .customMenu .gotoSlide').click (
     
     $e => {
         $e.preventDefault();
         let slide;
         const slideSection = $e.target.dataset.slideSection;
         
         if (!$e.target.dataset.slide) {
             const parentSlide = $($e.target).closest('.section')[0];
             slide = parentSlide.dataset.slide;
         } else {
             slide = $e.target.dataset.slide
         }

        //  if (slide && slideSection ) {
        //      sessionStorage.setItem('slideSection', slideSection);
        //      setTimeout(() => {
        //          com.gsk.mt.gotoSlide(slide);
        //      }, 10);
             
        //  } else 
         
         if (slide) {
             com.gsk.mt.gotoSlide(slide);
         }
     }
 );




