// --- local.js --- //

com.gsk.mt.onInit = function() {
	$(".isi-right #collapse-btn").trigger("click");
    $(".isi-right #expand-btn").show();
    $(".isi-right #collapse-btn").hide();

    com.gsk.mt.customSwipe({
        "rightSlide": "DLLEDTL200002_DOVATO_2020_US5.0_010"
	});
};

$(document).ready(function() {
    $("#container").on("swipeleft",function(){
        $(this).addClass("noSwipe");
    });

    $("#container").on("swiperight",function(){
        $(this).removeClass("noSwipe");
    });

    // Bumper logic
    $("#bumper_1_button").on("click", function() {
        $("#bumper_1").addClass("hidden");
        $("#bumper_2").removeClass("hidden"); // Hide bumper 1, Show bumper 2
        $(".h1-header--tab").removeClass("active");
        $("#tab_1_header").addClass("active"); // Active h1 header
        $(".side-menu__item").removeClass("active");
        $("#tab_1_button").addClass("active");  // Active button
        $(".tab").removeClass("active");
        $(".tab-1").addClass("active"); // Active bumper content
        $(".bumber-subtext").addClass("hidden");
        $("#container").addClass("noSwipe"); // to allow for custom swiping
    });

    $("#bumper_2_button").on("click", function() {
        $("#bumper_1").addClass("hidden");
        $("#bumper_2").removeClass("hidden"); // Hide bumper 1, Show bumper 2
        $(".h1-header--tab").removeClass("active");  // Active h1 header
        $("#tab_2_header").addClass("active");
        $(".side-menu__item").removeClass("active");
        $("#tab_2_button").addClass("active");  // Active button
        $(".tab").removeClass("active");
        $(".tab-2").addClass("active"); // Active bumper content
        $(".bumber-subtext").addClass("hidden");

        $("#container").addClass("noSwipe"); // to allow for custom swiping
    });

     // Custom Swipe logic
     $("#bumper_2").on("swiperight",function(){
       $("#bumper_2").addClass("hidden");
       $("#bumper_1").removeClass("hidden");
       $("#container").removeClass("noSwipe");
      // console.log("swipe right");
    });

    $("#bumper_2").on("swipeleft",function(){

        $("#container").removeClass("noSwipe");
         // to next KM
         com.gsk.mt.customSwipe({
             "leftSlide": "PM-US-DLL-EDTL-180002_DOVATO_2019_US2.0_013"
         });

     });


    $("#bumper_1").on("swipeleft",function(){
        $("#container").removeClass("noSwipe");
    });

    $("#bumper_1").on("swiperight",function(){
        $("#container").removeClass("noSwipe");
    });

		$('#bumper_1_button').on('click', function(){
			$('.mainContent').css('background-image', 'url("media/images/background.png")')
			$('.mainContent').css('background-position', '0px -80px')

		});
		$('#bumper_2_button').on('click', function(){
			$('.mainContent').css('background-image', 'url("media/images/background.png")')
			$('.mainContent').css('background-position', '0px -80px')
		});
		$('.side-menu__item').on('click', function(){
			if ( $('div.tab-2').hasClass('active') ) {
				$('.mainContent').css('background-position', '0px -80px')
			} else if ( $('div.tab-1').hasClass('active') ) {
				$('.mainContent').css('background-position', '0px -80px')
			}
		});
});

// Custome menu code
$(document).ready(function() { com.gsk.mt.initialise(); });
$('#customMenu .customMenu .gotoSlide').click (

     $e => {
         $e.preventDefault();
         let slide;
         const slideSection = $e.target.dataset.slideSection;

         if (!$e.target.dataset.slide) {
             const parentSlide = $($e.target).closest('.section')[0];
             slide = parentSlide.dataset.slide;
         } else {
             slide = $e.target.dataset.slide
         }

        //  if (slide && slideSection ) {
        //      sessionStorage.setItem('slideSection', slideSection);
        //      setTimeout(() => {
        //          com.gsk.mt.gotoSlide(slide);
        //      }, 10);

        //  } else 
         
         if (slide) {
             com.gsk.mt.gotoSlide(slide);
         }
     }
 );
