// --- local.js --- //

com.gsk.mt.onInit = function() {

	// console.log('on init');
	//$('#customMenu .customMenu').load('../shared/media/content/custom-menu.html');

 
	$(".isi-right #collapse-btn").trigger("click");
	$(".isi-right #expand-btn").show();
	$(".isi-right #collapse-btn").hide();

	com.gsk.mt.customSwipe({
        "leftSlide": "DLLEDTL200002_DOVATO_2020_US5.0_001"
	});
};

// Custom menu logic
$(document).ready(function() { 
   com.gsk.mt.initialise(); 

   if (com.veeva.clm.isEngage()) {
      let targetId = '#opening-loop';
      let $img = $('<img />', {
           class: 'poster-fallback',
           src: 'media/images/ViiV_CinemaGraph_poster.jpg' 
      })

      $img.insertBefore(targetId);
      $(targetId).remove();

 }

});
$('#customMenu .customMenu .gotoSlide').click (
     
     $e => {
         $e.preventDefault();
         let slide;
         const slideSection = $e.target.dataset.slideSection;
         
         if (!$e.target.dataset.slide) {
            const parentSlide = $($e.target).closest('.section')[0];
            slide = parentSlide.dataset.slide;
         } else {
            slide = $e.target.dataset.slide
         }

         // if (slide && slideSection ) {
         //    sessionStorage.setItem('slideSection', slideSection);
         //    setTimeout(() => {
         //        com.gsk.mt.gotoSlide(slide);
         //    }, 10);
             
         // } else 
         if (slide) {
            com.gsk.mt.gotoSlide(slide);
         }
     }
 );