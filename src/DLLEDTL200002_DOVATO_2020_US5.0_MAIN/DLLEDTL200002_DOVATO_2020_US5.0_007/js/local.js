// --- local.js --- //

com.gsk.mt.onInit = function() {
	$(".isi-right #collapse-btn").trigger("click");
    $(".isi-right #expand-btn").show();
    $(".isi-right #collapse-btn").hide();
    
    com.gsk.mt.customSwipe({
        "rightSlide": "DLLEDTL200002_DOVATO_2020_US5.0_005",
        "leftSlide": "DLLEDTL200002_DOVATO_2020_US5.0_007"
	});
};

// Custome menu code
$(document).ready(function() { com.gsk.mt.initialise(); });
$('#customMenu .customMenu .gotoSlide').click (
     
     $e => {
         $e.preventDefault();
         let slide;
         const slideSection = $e.target.dataset.slideSection;
         
         if (!$e.target.dataset.slide) {
             const parentSlide = $($e.target).closest('.section')[0];
             slide = parentSlide.dataset.slide;
         } else {
             slide = $e.target.dataset.slide
         }

        //  if (slide && slideSection ) {
        //      sessionStorage.setItem('slideSection', slideSection);
        //      setTimeout(() => {
        //          com.gsk.mt.gotoSlide(slide);
        //      }, 10);
             
        //  } else 
         
         if (slide) {
            com.gsk.mt.gotoSlide(slide);
         }
     }
 );


