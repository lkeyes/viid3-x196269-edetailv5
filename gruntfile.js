/*jslint devel: true */

/*global
module
*/


module.exports = function (grunt) {
    'use strict';

    var port = grunt.option('port') || 9001;
    var livereload = Number(grunt.option('livereloadport')) || true
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // sass: {
        //     dist: {
        //         options: {
        //             noCache: true
        //         },
        //         files: [{
        //             expand: true,
        //             src: ['**/*.scss'],
        //             ext: '.css'
        //         }]
        //     }
        // },

        sass: {
            dist: {
                options: {
                    noCache: false
                },
                files: [{
                    expand: true,
                    src: ['src/**/*.scss','!node_modules/**/*.scss'],
                    ext: '.css'
                }]
            }
        },
        
        connect: {
            server: {
                options: {
                    port,
                    base: '.',
                    open: {
                        target: `http://localhost:${port}`
                    },
                    livereload,
                }
            }
        },
        watch: {
            css: {
                files: ['src/**/*.scss'],
                tasks: ['sass'],
                options: {
                    livereload,
                },
            },
            html: {
                files: ['src/**/*.html'],
                //   tasks: [''],
                options: {
                    livereload,
                },
            },
            js: {
                files: ['src/**/*.js'],
                //   tasks: [''],
                options: {
                    livereload,
                },
            }
        },
        prompt: {
            collectGSKConfig: {
                options: {
                    questions: [
                        // {
                        //     config: 'pkg.GSKConfig.name', // arbitrary name or config for any other grunt task
                        //     type: '<question type>', // list, checkbox, confirm, input, password
                        //     message: 'String|function(answers)', // Question to ask the user, function needs to return a string,
                        //     default: 'value', // default value if nothing is entered
                        //     choices: 'Array|function(answers)',
                        //     validate: function(value), // return true if valid, error message if invalid. works only with type:input 
                        //     filter:  function(value), // modify the answer
                        //     when: function(answers) // only ask this question when this function returns true
                        //   }
                        {
                            config: 'pkg.GSKConfig.brand',
                            type: 'input',
                            message: 'Brand/Portfolio/Corporate: ',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        },
                        {
                            config: 'pkg.GSKConfig.prismTitle',
                            type: 'input',
                            message: 'PRISM Title (max 40 char): ',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        },
                        {
                            config: 'pkg.GSKConfig.year',
                            type: 'input',
                            message: 'Year: ',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        },
                        {
                            config: 'pkg.GSKConfig.version',
                            type: 'input',
                            message: 'Version: ',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        },
                        {
                            config: 'pkg.GSKConfig.prismId',
                            type: 'input',
                            message: 'PRISM ID: ',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        },
                        {
                            config: 'pkg.GSKConfig.productId',
                            type: 'input',
                            message: 'Product ID:',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        },
                        {
                            config: 'pkg.GSKConfig.plannerId',
                            type: 'input',
                            message: 'Planner ID:',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        },
                        {
                            config: 'pkg.GSKConfig.sellingTeam',
                            type: 'input',
                            message: 'Selling Team: ',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        },
                        {
                            config: 'pkg.GSKConfig.piFilename',
                            type: 'input',
                            message: 'PI File name on GSKSource (do not include file extension): ',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        },
                        {
                            config: 'pkg.GSKConfig.country',
                            type: 'input',
                            message: 'Country (ISO format): ',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        }
                    ]
                }
            },
            confirmGSKConfig: {
                options: {
                    questions: [
                        {
                            config: 'pkg.GSKConfig.configCorrect',
                            type: 'confirm',
                            message: 'Is this configuration correct?: ',
                        },
                    ],
                    then: function (results, done) {
                        if (!results['pkg.GSKConfig.configCorrect']) {
                            grunt.task.run('prompt:collectGSKConfig');
                        }
                    }
                }
            },
            addKeyMessage: {
                options: {
                    questions: [
                        {
                            config: 'newKeyMessage.title',
                            type: 'input',
                            message: 'Key Message Title: ',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        },
                        {
                            config: 'newKeyMessage.desc',
                            type: 'input',
                            message: 'Key Message Description: ',
                            validate: function (value) {

                                var valid;
                                if (value !== '') {
                                    valid = true;
                                }
                                return !!valid || 'Value can not be empty';
                            }
                        },

                    ],
                    then: function (results, done) {
                        let GSKConfig = grunt.config.get().pkg.GSKConfig;
                        let newKeyMessage = grunt.config.get().newKeyMessage;
                        GSKConfig.keymessages = GSKConfig.keymessages || [];
                        GSKConfig.keymessages.push(newKeyMessage);
                        grunt.config.set('pkg.GSKConfig', GSKConfig);

                        let newPathPrefix = GSKConfig.brand + '_' + GSKConfig.year + '_' + GSKConfig.country + GSKConfig.version;
                        if (GSKConfig.country.toLowerCase() === 'us') {
                            newPathPrefix = GSKConfig.plannerId + '_' + newPathPrefix;
                        }
                        let kmIndex = GSKConfig.keymessages.length - 1;
                        let cwd = 'MT_Scaffolding/MASTER_BLANK_MAIN/MT_BLANK_GSK_000';
                        let src = '**';
                        let dest = 'src/' + newPathPrefix + '_MAIN/' + newPathPrefix + '_' + pad(kmIndex, 3);


                        // copy the scaffolding and rename
                        grunt.config.set('copy.' + 'km' + kmIndex + '.expand', true);
                        grunt.config.set('copy.' + 'km' + kmIndex + '.cwd', cwd);
                        grunt.config.set('copy.' + 'km' + kmIndex + '.src', src);
                        grunt.config.set('copy.' + 'km' + kmIndex + '.dest', dest);

                        grunt.task.run('copy:' + 'km' + kmIndex);
                        grunt.task.run('prompt:addAnotherKeyMessagePrompt');
                    }
                }
            },
            addAnotherKeyMessagePrompt: {
                options: {
                    questions: [
                        {
                            config: 'addKeyMessageComplete',
                            message: 'Add another key message?',
                            type: 'confirm'
                        }
                    ],
                    then: function (results, done) {
                        console.log(results.addKeyMessageComplete);
                        if (results.addKeyMessageComplete) {
                            grunt.task.run('prompt:addKeyMessage');
                        } else {
                            grunt.task.run('saveSelections');
                        }
                    }
                }
            }
        }

    });

    // Load the plugins.
    grunt.loadNpmTasks('grunt-zip');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-menu');
    grunt.loadNpmTasks('grunt-prompt');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-connect');


    // Default task(s).
    grunt.registerTask('default', ['menu']);

    // Task Groups
    grunt.registerTask('initializeGSKProject', 'This task will collect GSK configuration variables and scaffold out a project structure based on the entered values using the GSK Master Template', ['prompt:collectGSKConfig', 'prompt:confirmGSKConfig', 'copyGSKScaffolding', 'copy', 'addKeyMessage']);
    grunt.registerTask('serve', 'This task will spin up a web server and then watch for file changes and trigger a live reload', ['sass', 'connect:server', 'watch'])
    grunt.registerTask('build', 'This task configures and runs the zip process for KM files', ['sass', 'configureKMZip', 'zip']);

    //Individual tasks

    // new task template
    // grunt.registerTask('aTask', '', function(){
    //     var done = this.async();
    //     done();
    // });

    grunt.registerTask('copyGSKScaffolding', 'This task will copy the MT scaffolding files into the src folder', function () {

        var done = this.async();
        let GSKConfig = grunt.config.get().pkg.GSKConfig;

        let newPathPrefix = GSKConfig.brand + '_' + GSKConfig.year + '_' + GSKConfig.country + GSKConfig.version;
        if (GSKConfig.country.toLowerCase() === 'us') {
            newPathPrefix = GSKConfig.plannerId + '_' + newPathPrefix;
        }

        grunt.file.expand({
            filter: 'isDirectory',
            cwd: 'MT_Scaffolding'
        }, '*')
            // iterate over each root path and build a copy function that modifies the path for the destination files
            .forEach((path, idx) => {
                let pathSuffix = path.substring(path.lastIndexOf('_'), path.length);

                let modifiedPath = newPathPrefix + pathSuffix;

                let cwd = 'MT_Scaffolding/' + path;
                let src = '**';
                let dest = 'src/' + modifiedPath;

                // caveat, this can't handle file names in the root of the cwd with no extension. so don't do that. succa.
                grunt.config.set('copy.' + idx + '.expand', true);
                grunt.config.set('copy.' + idx + '.cwd', cwd);
                grunt.config.set('copy.' + idx + '.src', '**');
                grunt.config.set('copy.' + idx + '.dest', dest);
                grunt.config.set('copy.' + idx + '.rename', function (dest, src) {
                    let modifiedSubPath;
                    if (src.indexOf('shared') === -1) {
                        modifiedSubPath = newPathPrefix + src.substring(src.indexOf('_'));
                    } else {
                        modifiedSubPath = src;
                    }
                    return dest + '/' + modifiedSubPath;
                });
            });



        // grunt.log.writeln(JSON.stringify(grunt.config.get().copy));

        done();
    });


    grunt.registerTask('saveSelections', 'This task will write the captured project values to the package.json file', function () {
        let done = this.async();

        let com = {
            gsk: {}
        }
        let GSKConfig = grunt.config.get().pkg.GSKConfig;
        // console.log(GSKConfig);
        let newPathPrefix = GSKConfig.brand + '_' + GSKConfig.year + '_' + GSKConfig.country + GSKConfig.version;
        if (GSKConfig.country.toLowerCase() === 'us') {
            newPathPrefix = GSKConfig.plannerId + '_' + newPathPrefix;
        }

        let configFilePath = 'src/' + newPathPrefix + '_MAIN/shared/js/config.json';

        let configFile = grunt.file.read(configFilePath);
        // console.log(configFile);
        // grab the com.gsk.mtconfig portion of the config file
        let gskConfigChunk = configFile.match(/com\.gsk\.mtconfig = {[\s\S]*[\r\n]};/) || configFile.match(/com\.gsk\.mtconfig = .*/);
        // separate the gakconfig chunk from the beginning of the file
        let prunedConfigFile = configFile.replace(gskConfigChunk, '');

        eval(gskConfigChunk[0]);

        com.gsk.mtconfig.pagesAll = [];
        com.gsk.mtconfig.pagesTitles = [];
        com.gsk.mtconfig.pagesDesc = [];

        // set main presentation
        com.gsk.mtconfig.presentation = newPathPrefix + '_MAIN';

        com.gsk.mtconfig.menuPresentation = newPathPrefix + '_ADD';
        com.gsk.mtconfig.menu = newPathPrefix + '_MENU';
        
        com.gsk.mtconfig.referencesPresentation = newPathPrefix + '_ADD';
        com.gsk.mtconfig.references = newPathPrefix + '_REFS';
        
        com.gsk.mtconfig.piPresentation = newPathPrefix + '_ADD';
        com.gsk.mtconfig.pi = newPathPrefix + '_PI';

        com.gsk.mtconfig.objectionPresentation = newPathPrefix + '_ADD';
        com.gsk.mtconfig.objection = newPathPrefix + '_OBJECTION';

        // iterate over the defined key messages and overwrite the appropriate config.json fields
        GSKConfig.keymessages.forEach((km, idx) => {
            if (idx === 0) {
                com.gsk.mtconfig.homepage = newPathPrefix + '_' + pad(idx, 3);
            }
            com.gsk.mtconfig.pagesAll.push(newPathPrefix + '_' + pad(idx, 3));
            com.gsk.mtconfig.pagesTitles.push(km.title);
            com.gsk.mtconfig.pagesDesc.push(km.desc);
        });


        let modifiedGSKConfigChunk = 'com.gsk.mtconfig = ' + JSON.stringify(com.gsk.mtconfig) + ';';

        let newConfigFile = prunedConfigFile + modifiedGSKConfigChunk;

        grunt.file.write(configFilePath, newConfigFile);

        grunt.file.write('package.json', JSON.stringify(grunt.config.get().pkg));

        done();
    });

    grunt.registerTask('addKeyMessage', 'This task will scaffold out a new key message folder as well as make the appropriate changes to the config.json file', function () {
        let done = this.async();
        let newKeyMessage = grunt.config.get().newKeyMessage;
        let GSKConfig = grunt.config.get().pkg.GSKConfig;
        if (GSKConfig) {
            grunt.task.run('prompt:addKeyMessage');
        } else {
            grunt.log.writeln('You do not appear to have a GSK config defined. Please run the initialization task to enable this option.')
        }
        done();
    });


    grunt.registerTask('configureKMZip', 'This task will create configurations dynamically based on the key messages available within the src folder', function () {
        var done = this.async();
        let GSKConfig = grunt.config.get().pkg.GSKConfig;
        let newPathPrefix = GSKConfig.brand + '_' + GSKConfig.year + '_' + GSKConfig.country + GSKConfig.version;
        if (GSKConfig.country.toLowerCase() === 'us') {
            newPathPrefix = GSKConfig.plannerId + '_' + newPathPrefix;
        }

        // this var lets you filter certain names (eg only folders with prefix)
        var src = newPathPrefix + '*';

        var folder_count = 0;
        var output_dir = 'dist';
        var output_file = '';
        var src_dir = 'src/' + newPathPrefix + '_MAIN';




        // This function iterates through the directories and creates a config object for each
        var km_folders = function () {
            grunt.file.expand({
                filter: 'isDirectory',
                // cwd: 'src/' + newPathPrefix + '_MAIN'
                cwd: src_dir
            }, src).forEach(function (path, index) {

                // grunt.log.writeln(path);



                output_file = output_dir + '/' + path + '.zip';
                // grunt.log.writeln('zipping ' + path + ' to ' + output_file );
                folder_count++;
                grunt.config.set('zip.' + folder_count + '.src', src_dir + '/' + path + '/**/*');
                grunt.config.set('zip.' + folder_count + '.dest', output_file);
                grunt.config.set('zip.' + folder_count + '.cwd', src_dir + '/' + path);
                // grunt.config.set('zip.' + folder_count + '.compression', 'DEFLATE');
            });

            return folder_count;

        };
        var count = km_folders();

        // This section outputs the shared directory as a zip
        var shared_dir = src_dir + '/shared';
        folder_count++;

        output_file = output_dir + '/' + newPathPrefix + '_Shared.zip';
        grunt.config.set('zip.' + folder_count + '.src', shared_dir + '/**/*');
        grunt.config.set('zip.' + folder_count + '.dest', output_file);
        grunt.config.set('zip.' + folder_count + '.cwd', shared_dir);


        grunt.log.writeln("Reading folders.  Found " + count);



        // console.log(grunt.config.get());
        done();
    });

    grunt.registerTask('generateVaultCSV', 'This task will create the CSV files required to load the key messages into Vault', function () {});



};



// HELPERS

// pad number with zeroes
function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}